var input = [[1, 2, 3, 4],
              [5, 6, 7, 8],
              [9, 10, 11, 12],
              [13, 14, 15, 16]];

console.log('\n start: \n', input);


function rotate(input){
  var row = 0, col=0,
    prev, curr,
    rowCount = input.length -1, colCount = input[0].length-1;

  while(row < rowCount && col < colCount){
    if(row + 1 > rowCount || col + 1 > colCount){
      break;
    }

    prev = input[row + 1][col];
    for (var i = col; i <=colCount; i++){
        curr = input[row][i];
        input[row][i] = prev;
        prev = curr;
    }
    row++;

    for(var i = row; i <= rowCount; i++){
      curr = input[i][colCount];
      input[i][rowCount] = prev;
      prev = curr;
    }
    colCount--;

    if(row <= rowCount){
      for (var i = colCount; i >= col; i--){
        curr = input[rowCount][i];
        input[rowCount][i] = prev;
        prev = curr;
      }
    }
    rowCount--;

    if(col <= colCount){
      for (var i = rowCount; i >= row; i--){
        curr = input[i][col];
        input[i][col] = prev;
        prev = curr;
      }
    }
    col++;
  }
  console.log( '\n end: \n',input);
}

rotate(input);